## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Explanation](#explanation)

## General info
Welcome to my GitLab profile! I am a full-stack programmer with 8 years of commercial experience.
I have a Master's degree in Computer Science from the Lodz University of Technology.
My areas of expertise include Java, JS, React, Spring, and Springboot.

In this project, I am participating in the ING "Zielona Tesla za zielony kod" competition,
where I have implemented three tasks (atmservice, onlinegame, transactions).
You can find out more about this competition on the ING website: (https://www.ing.pl/pionteching).

## Technologies
Project is created with:
* Quarkus version: 2.16.5.Final
* Java version: 17 (Oracle OpenJDK 17.0.2)
* Maven version: 3.6.3

## Setup
To run this project, install it locally using commands:

```
$ git clone https://gitlab.com/artur-dudzik/Z3jlzw4taw5n.git
$ cd ./Z3jlzw4taw5n
$ ./build.sh
$ ./run.sh
```

## Explanation
In this project, I utilized the Quarkus framework, a lightweight tool that enabled me to leverage Kubernetes' capabilities.
Moreover, using this framework, it is possible to compile the code to native GralVM code in the future,
resulting in faster application performance and lower memory requirements.

With my experience, I had no trouble getting started with Quarkus. The framework's intuitive design and excellent documentation made it easy to integrate into the project.
