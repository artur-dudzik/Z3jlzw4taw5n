#!/bin/bash

# Check if Java 17 is installed
if type -p java; then
    java_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
    if [[ "$java_version" < "17" ]]; then
        echo "Java version $(java -version) is older than the required version 17."
        exit 1
    else
        echo "Java 17 is installed correctly."
    fi
else
    echo "Java 17 is not installed. Please install Java 17 and try again."
    exit 1
fi

java -jar ./target/quarkus-app/quarkus-run.jar