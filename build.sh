#!/bin/bash

# Check if Java 17 is installed
if type -p java; then
    java_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
    if [[ "$java_version" < "17" ]]; then
        echo "Java version $(java -version) is older than the required version 17."
        exit 1
    else
        echo "Java 17 is installed correctly."
    fi
else
    echo "Java 17 is not installed. Please install Java 17 and try again."
    exit 1
fi

# Check if Maven is installed
if type -p mvn; then
    maven_version=$(mvn -version | grep "Apache Maven" | awk '{print $3}')
    if [[ "$maven_version" < "3.6.3" ]]; then
        echo "Maven version $maven_version is older than the required version 3.6.3."
        exit 1
    else
        echo "Maven 3.6.3 or newer is installed correctly."
    fi
else
    echo "Maven is not installed. Please install Maven 3.6.3 or newer and try again."
    exit 1
fi

mvn clean install

# Check if the build was successful
if [ $? -eq 0 ]; then
    echo -e "\n\033[1;32mBuild successful.\033[0m You can now run the application using the command \033[1m./run.sh\033[0m"
else
    echo -e "\n\033[1;31mBuild failed.\033[0m Cannot run the application."
fi