package ing.green.controller.impl;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class ATMApiTest {

    private final static String TESTED_ENDPOINT = "/atms/calculateOrder";
    private static final String EXAMPLE_JSON_REQUEST = "/requests/atm/atm_request_1.json";
    private static final String EXPECTED_JSON_RESPONSE = "/requests/atm/atm_response_1.json";
    private static final String EXAMPLE_JSON_REQUEST_2 = "/requests/atm/atm_request_2.json";
    private static final String EXPECTED_JSON_RESPONSE_2 = "/requests/atm/atm_response_2.json";
    private static final String EXAMPLE_INVALID_JSON_REQUEST = "/requests/atm/atm_invalid_request.json";

    @Test
    public void testCalculate_givenValidData_thenRetrieveValidResponse() throws IOException {
        String requestBody = readJsonFile(EXAMPLE_JSON_REQUEST);
        String expectedResponse = readJsonFile(EXPECTED_JSON_RESPONSE)
                .replaceAll("\\s+", "");
        given()
                .when()
                .body(requestBody).contentType(ContentType.JSON).post(TESTED_ENDPOINT)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(is(expectedResponse));
    }

    @Test
    public void testCalculate_givenValidSecondSecondDataSet_thenRetrieveValidResponse() throws IOException {
        String requestBody = readJsonFile(EXAMPLE_JSON_REQUEST_2);
        String expectedResponse = readJsonFile(EXPECTED_JSON_RESPONSE_2)
                .replaceAll("\\s+", "");
        given()
                .when()
                .body(requestBody).contentType(ContentType.JSON).post(TESTED_ENDPOINT)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(is(expectedResponse));
    }

    @Test
    public void testCalculate_givenInvalidData_thenReturnErrorResponse() throws IOException {
        String requestBody = readJsonFile(EXAMPLE_INVALID_JSON_REQUEST);
        given()
                .when()
                .body(requestBody).contentType(ContentType.JSON).post(TESTED_ENDPOINT)
                .then()
                .statusCode(400)
                .contentType(ContentType.JSON)
                .body("violations.size()", is(1))
                .body(
                        containsString("\"title\":\"Constraint Violation\""),
                        containsString("\"status\":400"),
                        containsString("\"violations\":[{")
                );
    }

    private String readJsonFile(String path) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(path).getFile());
        return new String(Files.readAllBytes(file.toPath()));
    }
}