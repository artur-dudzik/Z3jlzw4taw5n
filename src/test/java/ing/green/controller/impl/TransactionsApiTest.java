package ing.green.controller.impl;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class TransactionsApiTest {

    private final static String TESTED_ENDPOINT = "/transactions/report";
    private static final String EXAMPLE_JSON_REQUEST = "/requests/transactions/transactions_request.json";
    private static final String EXPECTED_JSON_RESPONSE = "/requests/transactions/transactions_response.json";
    private static final String EXAMPLE_INVALID_JSON_REQUEST = "/requests/transactions/transactions_invalid_request.json";


    @Test
    public void testCalculateOrderEndpoint() throws IOException {
        String requestBody = readJsonFile(EXAMPLE_JSON_REQUEST);
        String expectedResponse = readJsonFile(EXPECTED_JSON_RESPONSE)
                .replaceAll("\\s+", "");
        given()
                .when()
                .body(requestBody).contentType(ContentType.JSON).post(TESTED_ENDPOINT)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(is(expectedResponse));
    }

    @Test
    public void testCalculateClansEndpoint_givenInvalidRequest_thenRetrieveBadRequestResponse() throws IOException {
        String requestBody = readJsonFile(EXAMPLE_INVALID_JSON_REQUEST);

        given()
                .when()
                .body(requestBody).contentType(ContentType.JSON).post(TESTED_ENDPOINT)
                .then()
                .statusCode(400)
                .body("violations.size()", is(1))
                .body(
                        containsString("\"title\":\"Constraint Violation\""),
                        containsString("\"status\":400"),
                        containsString("\"violations\":[{")
                );
    }

    private String readJsonFile(String path) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(path).getFile());
        return new String(Files.readAllBytes(file.toPath()));
    }
}