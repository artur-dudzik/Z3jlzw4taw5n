package ing.green.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation for validating the "Players" object.
 * This annotation specifies that the "PlayersValidator" class should be used to validate the object.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PlayersValidator.class)
public @interface ValidPlayers {

    String message() default "Invalid Players object";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}