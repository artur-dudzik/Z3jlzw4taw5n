package ing.green.validation;

import ing.green.model.Players;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A validator for validating the "Players" object.
 * This validator checks that the number of players in each clan does not exceed the group count.
 */
@ApplicationScoped
public class PlayersValidator implements ConstraintValidator<ValidPlayers, Players> {

    @Override
    public boolean isValid(Players players, ConstraintValidatorContext context) {
        if (players == null || players.clans() == null) {
            return true; // null values are handled by @NotNull constraint
        }
        final int groupCount = players.groupCount();
        return players.clans().stream()
                .noneMatch(clan -> clan.numberOfPlayers() > groupCount);
    }
}