package ing.green.controller;

import ing.green.model.ATM;
import ing.green.model.Task;
import ing.green.service.ATMService;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;


/**
 * The controller class for handling ATM API requests
 * related to calculating orders for tasks.
 */
@Path("/atms/calculateOrder")
@Consumes({"application/json"})
@Produces({"application/json"})
@ApplicationScoped
public class ATMApi {

    private static final Logger LOG = Logger.getLogger(ATMApi.class);

    private final ATMService atmService;

    public ATMApi(final ATMService atmService) {
        this.atmService = atmService;
    }

    /**
     * Handles POST requests to calculate the order of ATMs to visit for a list of tasks.
     *
     * @param tasks the list of tasks to calculate the order of ATMs for
     * @return a list of ATMs in the order they should be visited to complete the tasks
     */
    @POST
    public List<ATM> calculate(@Valid List<Task> tasks) {
        LOG.debugf("Retrieved request with body size %d ", tasks.size());
        return atmService.calculate(tasks);
    }

}
