package ing.green.controller;

import ing.green.model.Account;
import ing.green.model.Transaction;
import ing.green.service.TransactionsService;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * The controller class for handling Transactions API
 * requests related to generating reports for transactions.
 */
@Path("/transactions/report")
@Consumes({"application/json"})
@Produces({"application/json"})
@ApplicationScoped
public class TransactionsApi {

    private static final Logger LOG = Logger.getLogger(TransactionsApi.class);

    private final TransactionsService transactionsService;

    public TransactionsApi(final TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    /**
     * Handles POST requests to generate a report for a list of transactions.
     *
     * @param transactions the list of transactions to generate a report for
     * @return a list of accounts with transaction details for the given transactions
     */
    @POST
    public List<Account> report(@Valid
                                @Size(max = 100000)
                                List<Transaction> transactions) {
        LOG.debugf("Retrieved request with body size %d ", transactions.size());
        return transactionsService.report(transactions);
    }
}
