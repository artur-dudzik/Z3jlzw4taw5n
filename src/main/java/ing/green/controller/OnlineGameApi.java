package ing.green.controller;

import ing.green.model.Clan;
import ing.green.model.Players;
import ing.green.service.OnlineGameService;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * The controller class for handling online game API
 * requests related to calculating player clans.
 */
@Path("/onlinegame/calculate")
@Consumes({"application/json"})
@Produces({"application/json"})
@ApplicationScoped
public class OnlineGameApi {

    private static final Logger LOG = Logger.getLogger(OnlineGameApi.class);

    private final OnlineGameService onlineGameService;

    public OnlineGameApi(final OnlineGameService onlineGameService) {
        this.onlineGameService = onlineGameService;
    }

    /**
     * Handles POST requests to calculate the clans of a list of players.
     *
     * @param players the list of players to calculate clans for
     * @return a list of lists of clans for each player in the input list
     */
    @POST
    public List<List<Clan>> calculate(@Valid Players players) {
        LOG.debugf("Retrieved request with clans size %d ", players.clans().size());
        return onlineGameService.calculate(players);
    }

}
