package ing.green.service;

import ing.green.model.ATM;
import ing.green.model.Task;

import java.util.List;

/**
 * Interface defines operations related to ATM service.
 */
public interface ATMService {

    /**
     * Calculates the ATMs based on the provided list of tasks.
     *
     * @param tasks a list of Task objects to be processed
     * @return a list of ATM objects
     */
    List<ATM> calculate(List<Task> tasks);
}
