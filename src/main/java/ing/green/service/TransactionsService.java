package ing.green.service;


import ing.green.model.Account;
import ing.green.model.Transaction;

import java.util.List;

/**
 * Interface defines operations related to transactions.
 */
public interface TransactionsService {

    /**
     * Creates a report of accounts with their transaction details from the given list of transactions.
     *
     * @param transactions a list of transactions to report
     * @return a list of accounts with their transaction details
     */
    List<Account> report(List<Transaction> transactions);

}
