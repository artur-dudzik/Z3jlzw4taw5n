package ing.green.service.impl;

import ing.green.model.Clan;
import ing.green.model.Players;
import ing.green.service.OnlineGameService;
import ing.green.service.impl.model.Group;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the OnlineGameService interface.
 */
@ApplicationScoped
public class OnlineGameServiceImpl implements OnlineGameService {

    @Override
    public List<List<Clan>> calculate(Players players) {
        final Integer groupCount = players.groupCount();
        final List<List<Clan>> groups = new ArrayList<>();
        final List<Clan> clans = players.clans();

        final List<Group> availableGroups = new ArrayList<>();

        clans.sort(Clan.clansComparator());

        clans.forEach(clan ->
                getAvailableGroup(groupCount, availableGroups, clan)
                        .ifPresentOrElse(clanGroup -> {
                            clanGroup.addClan(clan);
                            //cleanup available group
                            if (groupCount.equals(clanGroup.getTotalPlayers())) {
                                availableGroups.remove(clanGroup);
                            }
                        }, () -> {
                            List<Clan> clanList = new ArrayList<>();
                            clanList.add(clan);
                            groups.add(clanList);
                            //manage available group
                            if (clan.numberOfPlayers() < groupCount) {
                                availableGroups.add(new Group(clanList));
                            }
                        }));

        return groups;
    }

    private Optional<Group> getAvailableGroup(Integer groupCount,
                                              List<Group> availableGroups,
                                              Clan clan) {

        // Better performance than for each loop
        for (int i = 0; i < availableGroups.size(); i++) {
            Group availableGroup = availableGroups.get(i);
            if ((groupCount - availableGroup.getTotalPlayers() - clan.numberOfPlayers()) >= 0) {
                return Optional.of(availableGroup);
            }
        }
        return Optional.empty();
    }

}
