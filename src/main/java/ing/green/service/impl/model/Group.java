package ing.green.service.impl.model;

import ing.green.model.Clan;

import java.util.List;
import java.util.Objects;

/**
 * A helper class for managing a group of clans and their total number of players.
 */
public class Group {

    private Integer totalPlayers;
    private List<Clan> clans;

    public Group(List<Clan> clans) {
        this.clans = clans;
        if (clans.size() == 1) {
            this.totalPlayers = clans.get(0)
                    .numberOfPlayers();
        } else {
            this.totalPlayers = clans.stream()
                    .mapToInt(Clan::numberOfPlayers)
                    .sum();
        }
    }

    public void setClans(List<Clan> clans) {
        this.clans = clans;
    }

    public Integer getTotalPlayers() {
        return totalPlayers;
    }

    public void setTotalPlayers(Integer totalPlayers) {
        this.totalPlayers = totalPlayers;
    }

    public void addClan(Clan clan) {
        clans.add(clan);
        totalPlayers += clan.numberOfPlayers();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group that = (Group) o;

        return Objects.equals(totalPlayers, that.totalPlayers);
    }

    @Override
    public int hashCode() {
        return totalPlayers != null ? totalPlayers.hashCode() : 0;
    }
}