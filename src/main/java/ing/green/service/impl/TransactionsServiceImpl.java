package ing.green.service.impl;

import ing.green.model.Account;
import ing.green.model.Transaction;
import ing.green.service.TransactionsService;

import javax.enterprise.context.ApplicationScoped;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The TransactionsServiceImpl class is an implementation of the TransactionsService interface
 * that provides methods to report transactions.
 */
@ApplicationScoped
public class TransactionsServiceImpl implements TransactionsService {

    @Override
    public List<Account> report(List<Transaction> transactions) {

        Map<String, Account> accounts = new ConcurrentHashMap<>();

        transactions
                .parallelStream()
                .forEach(transaction -> {
                    accounts.compute(transaction.creditAccount(), (key, account) -> {
                        if (account == null) {
                            return createAccount(transaction.creditAccount(),
                                    transaction.amount(), 0, 1);
                        }
                        return new Account(
                                account.account(),
                                account.debitCount(),
                                account.creditCount() + 1,
                                calculateBalance(account.balance(), transaction.amount())
                        );
                    });

                    accounts.compute(transaction.debitAccount(), (key, account) -> {
                        if (account == null) {
                            return createAccount(transaction.debitAccount(),
                                    transaction.amount().negate(), 1, 0);
                        }
                        return new Account(
                                account.account(),
                                account.debitCount() + 1,
                                account.creditCount(),
                                calculateBalance(account.balance(), transaction.amount()
                                        .negate())
                        );
                    });
                });

        return accounts.values()
                .stream()
                .sorted(Comparator.comparing(Account::account))
                .toList();
    }

    /**
     * Calculates the balance of an account by adding the current balance to the given amount and rounding to 2 decimal places.
     *
     * @param currentBalance a BigDecimal representing the current balance of the account
     * @param toAdd          a BigDecimal representing the amount to be added to the balance
     * @return a BigDecimal representing the new balance of the account
     */
    private BigDecimal calculateBalance(BigDecimal currentBalance, BigDecimal toAdd) {
        return currentBalance.add(toAdd)
                .setScale(2, RoundingMode.CEILING);
    }

    /**
     * Creates a new Account object with the given parameters and sets the balance to 2 decimal places.
     *
     * @param account     a String representing the name of the account
     * @param balance     a BigDecimal representing the initial balance of the account
     * @param debitCount  an int representing the number of debit transactions for the account
     * @param creditCount an int representing the number of credit transactions for the account
     * @return a new Account object with the given parameters and the balance set to 2 decimal places
     */
    private Account createAccount(String account,
                                  BigDecimal balance,
                                  int debitCount,
                                  int creditCount) {
        return new Account(account,
                debitCount,
                creditCount,
                balance.setScale(2, RoundingMode.CEILING));
    }
}
