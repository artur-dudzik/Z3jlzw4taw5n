package ing.green.service.impl;

import ing.green.model.ATM;
import ing.green.model.Task;
import ing.green.service.ATMService;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class implements the ATMService interface and provides methods to calculate ATMs.
 */
@ApplicationScoped
public class ATMServiceImpl implements ATMService {

    @Override
    public List<ATM> calculate(List<Task> tasks) {

        return tasks.stream()
                .sorted(Task.taskComparator())
                .distinct()
                .map(task -> new ATM(task.region(), task.atmId()))
                .collect(Collectors.toList());
    }
}
