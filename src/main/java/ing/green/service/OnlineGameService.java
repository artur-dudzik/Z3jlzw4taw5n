package ing.green.service;

import ing.green.model.Clan;
import ing.green.model.Players;

import java.util.List;

/**
 * Interface defines operations related to online game.
 */
public interface OnlineGameService {

    /**
     * Calculates the groups of players in clans for an online game.
     *
     * @param players a Players object containing a list of clans
     * @return a List of List of Clan objects, representing the groups of clans
     */
    List<List<Clan>> calculate(Players players);
}
