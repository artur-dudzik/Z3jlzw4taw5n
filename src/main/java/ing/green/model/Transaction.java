package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

/**
 * A record representing a transaction with debit and credit accounts and an amount.
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public record Transaction(
        @Length(min = 26, max = 26)
        @JsonProperty("debitAccount")
        String debitAccount,

        @Length(min = 26, max = 26)
        @JsonProperty("creditAccount")
        String creditAccount,

        @JsonProperty("amount")
        BigDecimal amount
) {
}