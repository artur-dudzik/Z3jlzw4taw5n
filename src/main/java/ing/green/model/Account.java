package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Represents an account with its balance and transaction counts.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record Account(@JsonProperty("account")
                      String account,

                      @JsonProperty("debitCount")
                      Integer debitCount,

                      @JsonProperty("creditCount")
                      Integer creditCount,

                      @JsonProperty("balance")
                      BigDecimal balance) {
}