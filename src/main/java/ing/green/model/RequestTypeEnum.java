package ing.green.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Comparator;

/**
 * An enumeration of request types with priorities.
 * The priorities of the request types are used for sorting purposes. The order of the constants
 * in this enumeration is not the same as the order of priorities. This enumeration also provides
 * JSON serialization and deserialization using the {@link JsonValue} and {@link JsonCreator}
 * annotations.
 */
public enum RequestTypeEnum {
    STANDARD("STANDARD"), PRIORITY("PRIORITY"), SIGNAL_LOW("SIGNAL_LOW"), FAILURE_RESTART("FAILURE_RESTART");

    // caching enum access
    private static final java.util.EnumSet<RequestTypeEnum> values = java.util.EnumSet.allOf(RequestTypeEnum.class);

    private final String value;

    RequestTypeEnum(String v) {
        value = v;
    }

    @JsonValue
    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Returns the {@code RequestTypeEnum} object corresponding to the specified string value.
     *
     * @param v The string value of the enum constant.
     * @return The {@code RequestTypeEnum} object corresponding to the specified string value.
     * @throws IllegalArgumentException If the specified string value does not correspond to any
     *                                  enum constant.
     */
    @JsonCreator
    public static RequestTypeEnum fromValue(String v) {
        for (RequestTypeEnum b : values) {
            if (String.valueOf(b.value).equalsIgnoreCase(v)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + v + "'");
    }

    /**
     * Comparator for sorting objects by priority.
     *
     * @return A comparator for sorting {@code RequestTypeEnum} objects by priority.
     */
    public static Comparator<RequestTypeEnum> getComparator() {
        return Comparator.comparing(RequestTypeEnum::getPriority);
    }

    /**
     * Returns the priority of the specified {@code RequestTypeEnum} object.
     *
     * @param requestTypeEnum The {@code RequestTypeEnum} object to get the priority of.
     * @return The priority of the specified {@code RequestTypeEnum} object.
     */
    private static int getPriority(RequestTypeEnum requestTypeEnum) {
        return switch (requestTypeEnum) {
            case STANDARD -> 4;
            case SIGNAL_LOW -> 3;
            case PRIORITY -> 2;
            case FAILURE_RESTART -> 1;
        };
    }
}