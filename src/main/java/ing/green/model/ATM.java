package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents an ATM machine located in a specific region.
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public record ATM(
        @JsonProperty("region")
        Integer region,
        @JsonProperty("atmId")
        Integer atmId) {
}