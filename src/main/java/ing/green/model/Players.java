package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ing.green.validation.ValidPlayers;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Represents a group of clans in a game with their player count.
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@ValidPlayers(message = "The number of players in a clan exceeds the maximum allowed for the given group count.")
public record Players(

        @Range(min = 1, max = 1000)
        @JsonProperty("groupCount")
        Integer groupCount,

        @Valid
        @Size(max = 20000)
        @JsonProperty("clans")
        List<Clan> clans
) {
}