package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.Objects;

/**
 * Represents a task for an ATM machine, containing the region, the type of request, and the ATM ID.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record Task(
        @Min(1)
        @Max(9999)
        @JsonProperty("region")
        Integer region,

        @NotNull
        @JsonProperty("requestType")
        RequestTypeEnum requestType,

        @Min(1)
        @Max(9999)
        @JsonProperty("atmId")
        Integer atmId) {

    /**
     * Returns a comparator for sorting tasks by region and then request type priority.
     *
     * @return a comparator for tasks
     */
    public static Comparator<Task> taskComparator() {
        return Comparator.comparing(Task::region)
                .thenComparing(Task::requestType,
                        RequestTypeEnum.getComparator());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (!Objects.equals(region, task.region)) return false;
        return Objects.equals(atmId, task.atmId);
    }

    @Override
    public int hashCode() {
        int result = region != null ? region.hashCode() : 0;
        result = 31 * result + (atmId != null ? atmId.hashCode() : 0);
        return result;
    }
}