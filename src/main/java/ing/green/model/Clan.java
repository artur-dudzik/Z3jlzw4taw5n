package ing.green.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Comparator;

/**
 * Represents a clan in a game with its number of players and points.
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public record Clan(
        @Min(1)
        @Max(100)
        @JsonProperty("numberOfPlayers")
        Integer numberOfPlayers,

        @Min(1)
        @Max(1000000)
        @JsonProperty("points")
        Integer points) {

    /**
     * A comparator for sorting clans by points and number of players.
     * The comparison is first based on the number of points accumulated by the clan, with
     * clans having more points being sorted first. If two clans have the same number of points,
     * the comparison is then based on the number of players in the clan, with smaller clans
     * being sorted first.
     *
     * @return A comparator for sorting clans by points and number of players.
     */
    public static Comparator<Clan> clansComparator() {
        return (o1, o2) -> {
            int comparePoints = o2.points().compareTo(o1.points());
            if (comparePoints == 0) {
                return o1.numberOfPlayers() - o2.numberOfPlayers();
            }
            return comparePoints;
        };
    }
}